﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV5
{
    class Product : IShipable
    {
        private double price;
        private string description;
        private double mass;
        public Product(string description, double price,double mass)
        {
            this.description = description;
            this.price = price;
            this.mass = mass;
        }
        public double Price { get { return this.price; } }
        public string Description(int depth = 0)
        {
            return new string(' ', depth) + this.description;
        }
        public double Mass { get { return this.mass; } }
    }
}
