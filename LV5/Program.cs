﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV5
{
    class Program
    {
        static void Main(string[] args)
        {
            Dataset data1 = new Dataset("CSV.txt");
            User firstUser = User.GenerateUser("Ivan");
            User secondUser = User.GenerateUser("Zoran");
            ProtectionProxyDataset proxy1 = new ProtectionProxyDataset(firstUser);
            ProtectionProxyDataset proxy2 = new ProtectionProxyDataset(secondUser);
            VirtualProxyDataset proxy3 = new VirtualProxyDataset("CSV.txt");
            DataConsolePrinter printer = new DataConsolePrinter();
            printer.Print(proxy1);
            Console.WriteLine();
            printer.Print(proxy2);
            Console.WriteLine();
            Console.WriteLine("Virtual: \n");
            printer.Print(proxy3);
        }
    }
}
