﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV5
{
    class ShippingService
    {
        double pricePerWeight;
        public ShippingService(double pricePerWeight) { this.pricePerWeight = pricePerWeight; }
        public double deliveryPrice (double mass)
        {
            return pricePerWeight * mass;
        }
    }
}
//drugi zadatak-class ShippingService